﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TitlesExercise.Controllers
{
    public class TitleDetailsViewModel
    {
        public Title Title { get; set; }
        public IEnumerable<Award> Awards { get; set; }
        public IEnumerable<OtherName> OtherNames { get; set; }
        public IEnumerable<StoryLine> StoryLines { get; set; }
        public IEnumerable<TitleGenreViewModel> Genres { get; set; }
        public IEnumerable<TitleParticipantViewModel> Participants { get; set; }
    }

    public class TitleGenreViewModel
    {
        public int TitleId { get; set; }
        public string GenreName { get; set; }
    }

    public class TitleParticipantViewModel
    {
        public int TitleId { get; set; }
        public string ParticipantName { get; set; }
        public string ParticipantType { get; set; }
        public bool IsKey { get; set; }
        public string RoleType { get; set; }
        public bool IsOnScreen { get; set; }
    }
    
    public class HomeController : Controller
    {
        private TitlesEntities db = new TitlesEntities();
        public ActionResult Index()
        {
            return View("Index");
        }
        
        [HttpPost]
        public ActionResult Index(string searchString = "")
        {
            var titles = db.Titles.Where(t => t.TitleName.Contains(searchString));
            if (titles.Count() == 0)
                ViewBag.Message = "No titles found containing " + searchString;
            return View("Index", titles.ToList());
        }

        public ActionResult TitleDetails(int titleId = 0)
        {
            var title = db.Titles.Where(t => t.TitleId == titleId);            
            var awards = db.Awards.Where(a => a.TitleId == titleId);
            var otherNames = db.OtherNames.Where(o => o.TitleId == titleId);
            var storyLines = db.StoryLines.Where(s => s.TitleId == titleId);

            var genres = db.TitleGenres.Join(db.Genres, t => t.GenreId, g => g.Id, (t, g) => new { t, g })
                .Select(tg => new TitleGenreViewModel
                {
                    TitleId = tg.t.TitleId,
                    GenreName = tg.g.Name
                }).Where(x => x.TitleId == titleId);

            var participants = db.TitleParticipants.Join(db.Participants, t => t.ParticipantId, p => p.Id, (t, p) => new { t, p })
                .Select(tp => new TitleParticipantViewModel
                {
                    TitleId = tp.t.TitleId,
                    ParticipantName = tp.p.Name,
                    ParticipantType = tp.p.ParticipantType,
                    IsKey = tp.t.IsKey,
                    RoleType = tp.t.RoleType,
                    IsOnScreen = tp.t.IsOnScreen
                }).Where(x => x.TitleId == titleId).OrderBy(z => z.RoleType);

            return View("TitleDetails", new TitleDetailsViewModel 
            { 
                Title = title.FirstOrDefault(),
                Awards = awards.ToList(),
                OtherNames = otherNames.ToList(),
                StoryLines = storyLines.ToList(),
                Genres = genres.ToList(),
                Participants = participants.ToList()
            });
        }
    }
}
