﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TitlesExercise;
using TitlesExercise.Controllers;

namespace TitlesExercise.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            HomeController controller = new HomeController();
            ViewResult result = controller.Index() as ViewResult;
            Assert.AreEqual("Index", result.ViewName);
        }

        [TestMethod]
        public void TitleDetails()
        {
            HomeController controller = new HomeController();
            ViewResult result = controller.TitleDetails() as ViewResult;
            Assert.AreEqual("TitleDetails", result.ViewName);
        }
    }
}
